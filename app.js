//This file contains all javascript that is required for the computerStore to work.

//Get reference to all elements needed from the DOM.
const bankButton = document.getElementById("bank-button");
const workButton = document.getElementById("work-button");
const getLoanButton = document.getElementById("getLoan-button");
const repayLoanButton = document.getElementById("repayLoan-button");
const buyButton = document.getElementById("buy-button");
const bankBalanceDisplay = document.getElementById("bankBalance-display");
const workBalanceDisplay = document.getElementById("workBalance-display");
const loanDisplay = document.getElementById("outstandingLoan-display");
const computerDropdown = document.getElementById("computer-dropdown");
const featuresUl = document.getElementById("features-ul");
const descriptionP = document.getElementById("description-p");
const computerImg = document.getElementById("computer-img");
const priceHead = document.getElementById("price-header");
const computerNameHeader = document.getElementById("computerName-header");

//Add event linsteners to dropdown list and all buttons.
bankButton.addEventListener("click", bank);
getLoanButton.addEventListener("click", getLoan);
workButton.addEventListener("click", work);
repayLoanButton.addEventListener("click", repayLoan);
buyButton.addEventListener("click", buy);
computerDropdown.addEventListener("change", changeComputer);

//initialize all mutable variables. These are global and are used by the functions.
let bankBalance = 200;
let workBalance = 0;
let outstandingLoan = 0;
let computers = [];
let currentComputer = [];

//Initialize some values in the html.
bankBalanceDisplay.innerHTML = "Bank balance: " + bankBalance + " kr";
workBalanceDisplay.innerHTML = "Pay: " + workBalance + " kr";
loanDisplay.innerHTML = "Loan: " + outstandingLoan + " kr";

//fetch and store computers from API, and then display first computer.
fetch("https://hickory-quilled-actress.glitch.me/computers")
    .then(response => response.json())
    .then(data => computers = data)
    .then(() => addComputersToSelect(computers))
    .then(() => displayComputer(computers[0]));


//Transfers work/salary balance to bank balance. If there is a loan, 10% goes to paying back the loan.
function bank() {
    if (outstandingLoan == 0) {
        bankBalance += workBalance;
        workBalance = 0;
    }
    else {
        if (0.1*workBalance >= outstandingLoan) {
            workBalance -= outstandingLoan;
            outstandingLoan = 0;
            bankBalance += workBalance;
            workBalance = 0;
            loanDisplay.hidden = true;
            repayLoanButton.hidden = true;
        }
        else {
            outstandingLoan -= 0.1*workBalance;
            bankBalance += 0.9*workBalance;
            workBalance = 0;
        }
    }
    bankBalanceDisplay.innerHTML = "Bank balance: " + bankBalance + " kr";
    workBalanceDisplay.innerHTML = "Pay: " + workBalance + " kr";
    loanDisplay.innerHTML = "Loan: " + outstandingLoan + " kr";
}

//Asks user for desired loan. Checks some conditions before granting desired loan.
//If granted, a new textfield and button is displayed on the screen.
function getLoan() {
    if (outstandingLoan > 0) {
        alert("You already have a loan. Pay it back before taking out a new loan.");
        return;
    }

    let userInput = prompt("Input desired loan amount as an integer:", "e.g. 200");
    if (userInput == null) return; //if user cancelled
    desiredLoan = parseInt(userInput);
    
    while (isNaN(desiredLoan)) {
        userInput = prompt("Please input an integer:", "e.g. 200");
        if (userInput == null) return; //if user cancelled
        desiredLoan = parseInt(userInput);
    }

    if (desiredLoan>2*bankBalance) {
        alert("You cannot get a loan greater than twice your bank account");
    }
    else {
        bankBalance += desiredLoan;
        outstandingLoan = desiredLoan;
        bankBalanceDisplay.innerHTML = "Bank balance: " + bankBalance + " kr";
        loanDisplay.innerHTML = "Loan: " + outstandingLoan + " kr";
        loanDisplay.hidden = false;
        repayLoanButton.hidden = false;
    }
}

//Increases work balance by 100.
function work() {
    workBalance += 100;
    workBalanceDisplay.innerHTML = "Pay: " + workBalance + " kr";
}

//Uses work balance to repay loan. Any extra goes to the bank balance.
function repayLoan() {
    if (workBalance >= outstandingLoan) {
        workBalance -= outstandingLoan;
        outstandingLoan = 0;
        bankBalance += workBalance;
        workBalance = 0;
        loanDisplay.hidden = true;
        repayLoanButton.hidden = true;
    }
    else {
        outstandingLoan -= workBalance;
        workBalance = 0;
    }
    bankBalanceDisplay.innerHTML = "Bank balance: " + bankBalance + " kr";
    workBalanceDisplay.innerHTML = "Pay: " + workBalance + " kr";
    loanDisplay.innerHTML = "Loan: " + outstandingLoan + " kr";
}

//Buys the computer displayed if there is sufficient funds in the bank balance.
function buy() {
    if (currentComputer.price > bankBalance) {
        alert("You cannot afford this laptop.");
    }
    else {
        bankBalance -= currentComputer.price;
        bankBalanceDisplay.innerHTML = "Bank balance: " + bankBalance + " kr";
        alert("Congratulations! You are now the owner of a new laptop.")
    }
}

//Adds computers to dropdown menu.
function addComputersToSelect(computers) {
    computers.forEach(computer => {
        const computerOption = document.createElement("option");
        computerOption.value = computer.id;
        computerOption.appendChild(document.createTextNode(computer.title));
        computerDropdown.appendChild(computerOption);
    })
}

//Handles change in dropdown menu, and displays new computer.
function changeComputer(e) {
    displayComputer(computers[e.target.selectedIndex]);
}

function displayComputer(computer) {
    currentComputer = computer;

    //display features
    featuresUl.innerHTML = ""; //clear previous features
    for (const elem of computer.specs) {
        const computerFeature = document.createElement("li");
        computerFeature.appendChild(document.createTextNode(elem));
        featuresUl.appendChild(computerFeature);
    }

    //display computer name
    computerNameHeader.innerHTML = computer.title;

    //display description
    descriptionP.innerHTML = computer.description;

    //display price
    priceHead.innerHTML = computer.price.toString() + " NOK"

    //display image
    computerImg.src = "https://hickory-quilled-actress.glitch.me/" + computer.image;

}

//Called when image fails to load. Changes file extension in image-url from jpg to png or vice versa
function alterImageUrl() {
    if (computerImg.src.slice(-3) == "jpg") {
        computerImg.src = computerImg.src.slice(0,-3).concat("png");
    }
    else if (computerImg.src.slice(-3) == "png") {
        computerImg.src = computerImg.src.slice(0,-3).concat("jpg");
    }
}
